<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Benuterdaten anzeigen</title>
</head>
<body>
<?php
//Variable suche. Überprüft, ob eine Suche mitgegeben wurde, dann wird dies übernommen, sonst leer.
$suche = isset($_POST['suche']) ? $_POST['suche'] : "";
?>
<div class="container-sm">
    <div class="h1"><h1>Benutzerdaten anzeigen</h1></div>

    <!--Ziel index.php und post Methode zum übermitteln der Daten-->
    <form id="form_user" action="index.php" method="post" class="row g-3">

        <!--Feld für Suche-->
        <div class="col-auto">
            <label for="suche" class="form-control-plaintext">Suche:</label>
        </div>

        <!--Feld für die Eingabe-->
        <div class="col-auto">
            <!--In dem PHP Feld werden die mitgegebenen Zeichen in HTML-Code umgewandelt-->
            <input id="suche" name="suche" type="text" class="form-control" value="<?= htmlspecialchars($suche) ?>">
        </div>

        <!--Button für Suche-->
        <div class="col-auto">
            <button name="submit" type="submit" value="true" class="btn btn-primary">Suchen</button>
        </div>

        <!--Butten für Leeren-->
        <div class="col-auto">
            <a href="index.php" class="btn btn-secondary">Leeren</a>
        </div>
    </form>

    <!--Tabelle erstellen-->
    <table class="table table-striped mt-2 table-responsive">
        <!--Tabellenkopf Farbwahl.-->
        <thead class="table-warning">
        <!--Spalten-->
        <tr>
            <th scope="col">Name</th>
            <th scope="col">E-mail</th>
            <th scope="col">Geburtsdatum</th>
        </tr>
        </thead>
        <tbody>


        <?php
        //einbeziehen der Datei
        include "lib/func.inc.php";
        //Wenn der Suchbutton gedrückt wird, wird die Methode printAllFiteredData aufgerufen.
        //Dieser Methode wird, dann eine eitere Methode getFilteredData und aus der POST Anweisung wird die 'suche'
        //mitgegeben. Ansonsten wird die Methode printAllData aufgerufen und dort wird die Methode getAllData mitgegeben.
        if (isset($_POST['submit'])) {
            printAllFilteredData(getFilteredData($_POST['suche']));
        } else {
            printAllData(getAllData());
        }
        ?>
        </tbody>

    </table>

</div>
</body>
</html>
