<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Benuterdetails</title>
</head>
<body>

<!--Container für die Benutzerdetails-->
<div class="container-sm">
    <div class="h1"><h1>Benuterdetails</h1></div>

    <!--Zurück Button-->
    <a href="index.php" class="btn btn-secondary">Zurück</a>


    <?php
    //einbeziehen der Datei
    include "lib/func.inc.php";
    //aufrufen der Methode printDataPerId und dort wird die Methode getDataPerId mitgegeben, mit der id aus der GET-Anfrage.
    printDataPerId(getDataPerId($_GET['id']));
    ?>

</div>
</body>
</html>

