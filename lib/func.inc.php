<?php

//Einbeziehen der Datei
include "userdata.php";

/**
 * Methode zum Erfassen aller Daten.
 * @return array[]|null Userdaten oder Null
 */
function getAllData()
{
    global $data;
    if (empty($data)) {
        return null;
    } else {
        return $data;
    }
}

/**
 * Methode zum finden eines Objekt mit der ID
 * @param $id der Person
 * @return array|null Person oder Null
 */
function getDataPerId($id)
{
    global $data;

    foreach ($data as $user) {
        if ($user['id'] == $id) {
            return $user;
        }
    }
    return null;
}

/**
 * Methode um nach Personen zu suchen und diese zu finden.
 * @param $filter Zeichenfolge wird mitgegeben
 * @return array Person
 */
function getFilteredData($filter)
{

    global $data;

    $filteredData = array();
    foreach ($data as $key => $value) {
        $name = $value['firstname'] . " " . $value['lastname'];
        $email = $value['email'];
        if (stripos($name, $filter) !== false || stripos($email, $filter) !== false) {
            $filteredData[] = $value;
        }
    }
    return $filteredData;
}

/**
 * Ausgabe aller Daten, die Daten erhalten wir in der index.html mit dem Aufruf der Methode getAllData.
 * @param $userdata getAllData alle Personen in der Datei userdata.php
 */
function printAllData($userdata)
{
    if ($userdata == null) {
        echo "<div class='alert alert-danger mt-2'><p>Keine Daten vorhanden!</p></div>";
        return;
    } else {
        foreach ($userdata as $key => $value) {
            echo "<tr>
            <td><a href='details.php?id=" . $value['id'] . "' class='text-decoration-none'>" . $value['firstname'] . " " . $value['lastname'] . "</a></td>
            <td>" . $value['email'] . "</td>
            <td>" . (new DateTime($value['birthdate']))->format('d.m.Y') . "</td>
            </tr>";
        }
    }
}

/**
 * Ausgabe der gefundenen Personen aus getFilteredData.
 * @param $filteredData getFilteredData Personen die man sucht
 */
function printAllFilteredData($filteredData)
{
    if (empty($filteredData)) {
        echo "<div class='alert alert-danger mt-2'><p>Es gibt keine Einträge mit diesem Suchbegriff!</p></div>";
    } else {
        foreach ($filteredData as $key => $value) {
            echo "<tr>
            <td><a href='details.php?id=" . $value['id'] . "' class='text-decoration-none'>" . $value['firstname'] . " " . $value['lastname'] . "</a></td>
            <td>" . $value['email'] . "</td>
            <td>" . (new DateTime($value['birthdate']))->format('d.m.Y') . "</td>
            </tr>";
        }
    }
}

/**
 * Ausgabe der gefundene Person aus getDataPerID
 * @param $idData getDataPerID ID der gesuchten Person
 * @throws Exception
 */
function printDataPerId($idData)
{

    if ($idData == null) {
        echo "<div class='alert alert-danger mt-2'><p>ID ist ungültig!</p></div>";
        return;
    } else {

        echo "<table class='table mt-2 table-responsive'>
        <tbody>
        <tr>
            <td>Vorname</a></td>
            <td>" . $idData['firstname'] . "</td>
        </tr>
        <tr>
            <td>Nachname</a></td>
            <td>" . $idData['lastname'] . "</td>
        </tr>
        <tr>
            <td>Geburtstag</a></td>
            <td>" . (new DateTime($idData['birthdate']))->format('d.m.Y') . "</td>
        </tr>
        <tr>
            <td>E-Mail</a></td>
            <td>" . $idData['email'] . "</td>
        </tr>
        <tr>
            <td>Telefon</a></td>
            <td>" . $idData['phone'] . "</td>
        </tr>
        <tr>
            <td>Straße</a></td>
            <td>" . $idData['street'] . "</td>
        </tr>
        </tbody>
    </table>";
      }
}

?>
